package it.unibo.oop.lab.exception2;

public class TransactionOverQuotaException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TransactionOverQuotaException() {
		super();
	}

}
