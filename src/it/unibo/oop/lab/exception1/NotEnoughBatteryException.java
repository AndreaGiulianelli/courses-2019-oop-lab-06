package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double batteryLevel;
	
	public NotEnoughBatteryException(final double batteryLevel) {
		super();
		this.batteryLevel = batteryLevel;
	}
	
	public String toString() {
		return super.toString() + " battery level: " + this.batteryLevel;
	}
	
}
