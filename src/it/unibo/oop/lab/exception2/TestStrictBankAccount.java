package it.unibo.oop.lab.exception2;

import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	AccountHolder a1 = new AccountHolder("Mario","Rossi",1);
    	AccountHolder a2 = new AccountHolder("Carlo","Giallo",2);
    	BankAccount b1 = new StrictBankAccount(a1.getUserID(),10000,10);
    	BankAccount b2 = new StrictBankAccount(a2.getUserID(),10000,10);
    	
    	try {
    		b1.deposit(a1.getUserID(), 10000);
    	} catch(WrongAccountHolderException e) {
    		fail();
    	}
    	
    	try {
    		b1.deposit(a2.getUserID(), 10000);
    		fail();
    	} catch(WrongAccountHolderException e) {
    		System.out.println("OK");
    	}
    	
    	try {
    		for(int i = 0; i < 12; i++) {
    			b2.depositFromATM(a2.getUserID(), 10000);
    		}
    		fail();
    	} catch(TransactionOverQuotaException e) {
    		System.out.println("OK");
    	}
    	
    	try {
    		for(int i = 0; i < 12; i++) {
    			b2.withdraw(a2.getUserID(), 10000);
    		}
    		fail();
    	} catch(NotEnoughFoundException e) {
    		System.out.println("OK");
    	}
    	
    	
    }
}