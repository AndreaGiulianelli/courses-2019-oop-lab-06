package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
    private static final int ELEMS = 100_000;
    private static final int LOWER_LIMIT_RANGE = 1000;
    private static final int UPPER_LIMIT_RANGE = 2000;
    private static final int READ_MIDDLE_INDEX_NUMBER_TIMES = 1000;
    private static final long AFRICA_POPULATION = 1110635000L;
	private static final long AMERICAS_POPULATION = 972005000L;
	private static final long ANTARTICA_POPULATION = 0L;
	private static final long ASIA_POPULATION = 4298723000L; 
	private static final long EUROPE_POPULATION = 742452000L;
	private static final long OCEANIA_POPULATION = 38304000L;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final List<Integer> numberList = new ArrayList<>();
    	for (int i = LOWER_LIMIT_RANGE; i < UPPER_LIMIT_RANGE; i++) {
    		numberList.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List<Integer> linkedNumberList = new LinkedList<>(numberList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int firstNumber = numberList.set(0, numberList.get(numberList.size()-1));
    	numberList.set(1, firstNumber);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (final int number : numberList) {
    		System.out.println(number);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime(); 
		for (int i = 0; i < ELEMS; i++) {
			numberList.add(0, i);
		}
		time = System.nanoTime() - time;
    	System.out.println("Insert " + ELEMS + " elements in the first index of a ArrayList: " + TimeUnit.NANOSECONDS.toMillis(time));
    	time = System.nanoTime();
    	for (int i = 0; i < ELEMS; i++) {
    		linkedNumberList.add(0, i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Insert " + ELEMS + " elements in the first index of a LinkedList: " + TimeUnit.NANOSECONDS.toMillis(time));
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	time = System.nanoTime();
    	final int middleIndex = numberList.size()/2;
    	for (int i = 0; i < LOWER_LIMIT_RANGE; i++) {
    		numberList.get(middleIndex);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Read middle element for " + READ_MIDDLE_INDEX_NUMBER_TIMES + " times of a ArrayList: " + TimeUnit.NANOSECONDS.toMillis(time));
    	time = System.nanoTime();
		final int middleIndexLinkedList = linkedNumberList.size() / 2;
    	for (int i = 0; i < LOWER_LIMIT_RANGE; i++) {
    		linkedNumberList.get(middleIndexLinkedList);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Read middle element for " + READ_MIDDLE_INDEX_NUMBER_TIMES + " times of a LinkedList: " + TimeUnit.NANOSECONDS.toMillis(time));
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	final Map<String,Long> continentsPopulation = new HashMap<>();
		continentsPopulation.put("Africa", AFRICA_POPULATION);
		continentsPopulation.put("Americas", AMERICAS_POPULATION);
		continentsPopulation.put("Antartica", ANTARTICA_POPULATION);
		continentsPopulation.put("Asia", ASIA_POPULATION);
		continentsPopulation.put("Europe", EUROPE_POPULATION);
		continentsPopulation.put("Oceania", OCEANIA_POPULATION);
        /*
         * 8) Compute the population of the world
         */
    	long worldPopulation = 0L;
    	for (final long continentPopulation : continentsPopulation.values()) {
    		worldPopulation = worldPopulation + continentPopulation;
    	}
    	System.out.println("World population is: " + worldPopulation);
    }
}
