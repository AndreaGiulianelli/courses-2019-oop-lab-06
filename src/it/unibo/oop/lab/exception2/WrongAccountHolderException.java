package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends IllegalArgumentException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int usrId;
	
	public WrongAccountHolderException(final int usrId) {
		super();
		this.usrId = usrId;
	}
	
	public String toString() {
		return super.toString() + "\t" + this.usrId;
	}

}
