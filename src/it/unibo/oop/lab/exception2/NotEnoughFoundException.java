package it.unibo.oop.lab.exception2;

public class NotEnoughFoundException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotEnoughFoundException() {
		super();
	}
	
}
